export default {
  state: {
    userData: [],
    userPermissions: []
  },
  getters: {
    getUserData(state) {
      return state.userData;
    },
    getUserPermissions(state) {
      return state.userPermissions;
    }
  },
  mutations: {
    mutateUserData(state, payload) {
      state.userData = payload;
    },
    mutateUserPermissions(state, payload) {
      state.userPermissions = payload;
    }
  },
  actions: {
    setUserData(context, data) {
      context.commit("mutateUserData", data);
    },
    setUserPermissions(context, data) {
      context.commit("mutateUserPermissions", data);
    }
  }
};
