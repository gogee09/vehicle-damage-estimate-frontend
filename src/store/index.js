import Vue from "vue";
import Vuex from "vuex";
import user from "./user";
Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    itemsToDelete: [],
    itemsToFilterBy: []
  },
  getters: {
    getItemsToDelete(state) {
      return state.itemsToDelete;
    },
    getItemsToFilterBy(state) {
      return state.itemsToFilterBy;
    }
  },
  mutations: {
    mutateItemsToDelete(state, payload) {
      state.itemsToDelete = payload;
    },
    mutateItemsToFilterBy(state, payload) {
      state.itemsToFilterBy = payload;
    }
  },
  actions: {
    setItemsToDelete(context, data) {
      context.commit("mutateItemsToDelete", data);
    },
    setItemsToFilterBy(context, data) {
      context.commit("mutateItemsToFilterBy", data);
    }
  },
  modules: {
    user
  }
});
