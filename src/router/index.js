import Vue from "vue";
import VueRouter from "vue-router";
import Counteragents from "@/views/app/counteragents/Counteragents";
import CounteragentEdit from "@/views/app/counteragents/CounteragentEdit";
import CounteragentCreate from "@/views/app/counteragents/CounteragentCreate";
import Vehicles from "@/views/app/vehicles/Vehicles";
import Cookies from "js-cookie";

Vue.use(VueRouter);

const routes = [
  {
    path: "/auhtentication",
    name: "Authentication",
    component: () =>
      import(
        /* webpackChunkName: "group-foo" */ "@/views/authentication/index"
      ),
    children: [
      {
        path: "/authentication/login",
        name: "login",
        component: () =>
          import(
            /* webpackChunkName: "group-foo" */ "@/views/authentication/loginView/LoginForm"
          )
      }
    ]
  },
  {
    path: "/",
    name: "HomePage",
    component: () =>
      import(/* webpackChunkName: "group-foo" */ "@/views/app/index"),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: "/counteragents",
        name: "Counteragents",
        component: Counteragents
      },
      {
        path: "/counteragents/create",
        name: "CounteragentCreate",
        component: CounteragentCreate
      },
      {
        path: "/counteragents/:id",
        name: "CounteragentEdit",
        component: CounteragentEdit
      },
      {
        path: "/vehicles",
        name: "Vehicles",
        component: Vehicles
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (Cookies.get("apollo-token")) {
      next();
      return;
    }
    next("/authentication/login");
  }
  next();
});

export default router;
