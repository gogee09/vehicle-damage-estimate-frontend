FROM node:lts-alpine

WORKDIR /untitled1

COPY package*.json ./

RUN npm install

COPY ./ ./

RUN npm run build

#COPY dist ./

EXPOSE 8080

CMD ["npm", "run", "serve"]